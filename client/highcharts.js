fetch('../output/data.json').then(response => response.json()).then((json) => {
    pieChartForNoOfTimesOlympicsHstd(json["noOfTimesOlympicsHstd"]);
    stackedColumnChartForTopTenCountries(json["topTenCountries"]);
    columnChartForMaleFemaleParticipation(json["maleAndFemaleParticipationByDecade"]);
    lineChartForAvgAgeOfAthletes(json["averageAgeOfAthletes"]);
    tableForAllMedalWinnersFromIndia(json["allMedalWinnersFromIndia"]);
});


function pieChartForNoOfTimesOlympicsHstd(city) {

    const jsonDataPieFormat = () => {
        const getDataInArray = [];

        for (let key in city) {
            let pieData = {
                name: key,
                y: city[key]
            }
            getDataInArray.push(pieData);
        }
        return getDataInArray;
    }


    // eslint-disable-next-line no-undef
    Highcharts.chart('container1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Number of Times Olympics Hosted Per Season'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: jsonDataPieFormat()
        }]
    });
}

function stackedColumnChartForTopTenCountries(countries) {

    const jsonDataStackedColumnFormat = () => {
        const getDataInArray = [];
        const medals = ['Gold', 'Silver', 'Bronze'];
        for (var medal in medals) {
            const valuesInArray = [];
            for (let item in countries) {
                valuesInArray.push(countries[item][medals[medal]])
            }
            let columnData = {
                name: medals[medal],
                data: valuesInArray
            }
            getDataInArray.push(columnData);
        }
        return getDataInArray;
    }

    //console.log(jsonDataStackedColumnFormat());
    const getCountries = () =>{
        const getDataInArray = [];
        for(let country in countries){
            getDataInArray.push(country);
        }
        return getDataInArray;
    }

    // eslint-disable-next-line no-undef
    Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top ten countries who won most medals'
        },
        xAxis: {
            categories: getCountries()
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Number Of Medals'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        // eslint-disable-next-line no-undef
                        Highcharts.defaultOptions.title.style &&
                        // eslint-disable-next-line no-undef
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            // eslint-disable-next-line no-undef
            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: jsonDataStackedColumnFormat()
    });
}

function columnChartForMaleFemaleParticipation(decades) {
    const getMaleDataInColumnFormat = () => {
        const getMaleDataInArray = [];
        for(let decade in decades) {
            getMaleDataInArray.push(decades[decade]['M']);
        }
        return getMaleDataInArray;
    }

    const getFeMaleDataInColumnFormat = () => {
        const getFeMaleDataInArray = [];
        for(let decade in decades) {
            getFeMaleDataInArray.push(decades[decade]['F']);
        }
        return getFeMaleDataInArray;
    }
    
    const getDecades = () => {
        var getDataInArray = [];
        for(let decade in decades){
            getDataInArray.push(decade);
        }
        return getDataInArray;
    }

    Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Male and female Participation by Decade'
        },
        xAxis: {
            categories: getDecades(),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Male',
            data: getMaleDataInColumnFormat()
    
        }, {
            name: 'Female',
            data: getFeMaleDataInColumnFormat()
    
        }]
    });
}

function lineChartForAvgAgeOfAthletes(years) {

    const jsonDataLineFormat = () => {
        const getDataInArray = [];
        for(let year in years) {
            getDataInArray.push(parseFloat(years[year]));
        }
        return getDataInArray;
    }
    const getYears = () => {
        const getDataInArray = [];
        for(let year in years) {
            getDataInArray.push(year);
        }
        return getDataInArray;
    }

    Highcharts.chart('container4', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Average Age Of Athletes'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: getYears()
        },
        yAxis: {
            title: {
                text: 'Average Ages'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        series: [{
            name: 'AverageAges',
            marker: {
                symbol: 'square'
            },
            data: jsonDataLineFormat()
    
        }]
    });
}

function tableForAllMedalWinnersFromIndia(season) {
 // eslint-disable-next-line no-console
 console.log(season)
}


