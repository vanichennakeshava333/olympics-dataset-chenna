const fs = require('fs');
const eventsData = JSON.parse(fs.readFileSync('../data/athlete_events.json').toString());
const regionsData = JSON.parse(fs.readFileSync('../data/noc_regions.json').toString());
import 

let jsonData = {
    noOfTymsOlympicsHstd: functions.noOfTimesOlympicsHstd(eventsData),
    topTenCountries: functions.topTenCountries(eventsData, regionsData),
    maleAndFemaleParticipationByDecade: functions.maleAndFemaleParticipationByDecade(eventsData),
    averageAgeOfAthletes: functions.averageAgeOfAthletes(eventsData),
    allMedalWinnersFromIndia: functions.allMedalWinnersFromIndia(eventsData)
};
fs.writeFile('data.json',
    JSON.stringify(jsonData, null, 5),
    err => {
        if (err) {
            // eslint-disable-next-line no-console
            console.log(err);
        }
    });
