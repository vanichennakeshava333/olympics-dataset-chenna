//1.Number of times olympics hosted per City over the years - Pie chart

export function noOfTimesOlympicsHstd(data) {
  const noOfTimesOlympicsHstdPerCity = data.reduce((allCities, obj) => {
    if (!allCities[obj["Games"]]) {
      allCities[obj["Games"]] = "Season";
      if (!allCities[obj["City"]]) {
        allCities[obj["City"]] = 1;
      } else {
        allCities[obj["City"]] += 1;
      }
    }
    return allCities;
  }, {});

  let noOfTimesOlympicsHstdPerCityIntoArray = Object.keys(
    noOfTimesOlympicsHstdPerCity
  ).map(obj => {
    return {
      [obj]: noOfTimesOlympicsHstdPerCity[obj]
    };
  });

  let filterUsingSeason = noOfTimesOlympicsHstdPerCityIntoArray.filter(obj => {
    return Object.values(obj) != "Season";
  });

  let groupingIntoSingleObject = Object.assign({}, ...filterUsingSeason);

  return groupingIntoSingleObject;
}
// console.log(noOfTimesOlympicsHstd(eventsData));
// console.log(noOfTimesOlympicsHstd());

//2.Top 10 countries who have won most medals after 2000 - stacked column - split gold/silver/bronze
export function topTenCountries(data, regionsData) {
  const topTenCountriesWonMedal = data.reduce((countries, obj) => {
    if (obj["Year"] > 2000 && obj["Medal"] != "NA") {
      if (countries.hasOwnProperty(obj["NOC"])) {
        countries[obj["NOC"]][obj["Medal"]] += 1;
        countries[obj["NOC"]].count += 1;
      } else {
        countries[obj["NOC"]] = {};
        countries[obj["NOC"]]["Gold"] = 0;
        countries[obj["NOC"]]["Silver"] = 0;
        countries[obj["NOC"]]["Bronze"] = 0;
        countries[obj["NOC"]][obj["Medal"]] = 1;
        countries[obj["NOC"]].count = 1;
      }
    }
    return countries;
  }, {});

  let sortedUsingCount = Object.keys(topTenCountriesWonMedal).sort(
    (min, max) => {
      return (
        topTenCountriesWonMedal[max].count - topTenCountriesWonMedal[min].count
      );
    }
  );

  let slicedToTopTenCountries = sortedUsingCount.slice(0, 10);

  var result = {};

  slicedToTopTenCountries.map(key => {
    delete topTenCountriesWonMedal[key].count;
    result[key] = topTenCountriesWonMedal[key];
  }, {});

  const getRegions = regionsData.reduce((acc, obj) => {
    acc[obj["NOC"]] = obj["region"];
    return acc;
  }, {});

  const regionWise = Object.keys(result).reduce((acc, key) => {
    acc[getRegions[key]] = result[key];
    return acc;
  }, {});

  return regionWise;
}
//console.log(topTenCountries());

// 3.M/F participation by decade - column chart

export function maleAndFemaleParticipationByDecade(data) {
  let yearWiseMaleandFemaleParticipants = data.reduce((years, obj) => {
    if (years.hasOwnProperty([obj["Year"]])) {
      if (years[obj["Year"]]["Ids"][obj["ID"]] != obj["Games"]) {
        years[obj["Year"]][obj["Sex"]] += 1;
        years[obj["Year"]]["Ids"][obj["ID"]] = obj["Games"];
      }
    } else {
      years[obj["Year"]] = {};
      years[obj["Year"]]["M"] = 0;
      years[obj["Year"]]["F"] = 0;
      years[obj["Year"]][obj["Sex"]] = 1;
      years[obj["Year"]]["Ids"] = {};
      years[obj["Year"]]["Ids"][obj["ID"]] = obj["Games"];
    }
    return years;
  }, {});

  Object.keys(yearWiseMaleandFemaleParticipants).map(key => {
    delete yearWiseMaleandFemaleParticipants[key]["Ids"];
  });

  var decades = Object.keys(yearWiseMaleandFemaleParticipants).reduce(
    (decade, years) => {
      let dec = years.substring(0, 3) + 0 + "-" + years.substring(0, 3) + 9;
      if (decade.hasOwnProperty(dec)) {
        decade[dec]["M"] += parseInt(
          yearWiseMaleandFemaleParticipants[years]["M"]
        );
        decade[dec]["F"] += parseInt(
          yearWiseMaleandFemaleParticipants[years]["F"]
        );
      } else {
        decade[dec] = {};
        decade[dec]["M"] = parseInt(
          yearWiseMaleandFemaleParticipants[years]["M"]
        );
        decade[dec]["F"] = parseInt(
          yearWiseMaleandFemaleParticipants[years]["F"]
        );
      }
      return decade;
    },
    {}
  );
  return decades;
}

// console.log(maleAndFemaleParticipationByDecade());

// 4.Per year average age of athletes who participated in Boxing Men’s Heavyweight - Line
export function averageAgeOfAthletes(data) {
  const sumOfAges = data.reduce((acc, cv) => {
    if (cv["Event"] == "Boxing Men's Heavyweight" && cv["Age"] != "NA") {
      if (acc.hasOwnProperty(cv["Year"])) {
        acc[cv["Year"]]["Sum"] += parseInt(cv["Age"]);
        acc[cv["Year"]].count += 1;
      } else {
        acc[cv["Year"]] = {};
        acc[cv["Year"]]["Sum"] = parseInt(cv["Age"]);
        acc[cv["Year"]].count = 1;
      }
    }
    return acc;
  }, {});
  Object.keys(sumOfAges).map(key => {
    sumOfAges[key] = parseFloat(
      sumOfAges[key]["Sum"] / sumOfAges[key]["count"]
    ).toFixed(2);
  });
  return sumOfAges;
}

//console.log(averageAgeOfAthletes());

//5.Find out all medal winners from India per season - Table
export function allMedalWinnersFromIndia(data) {
  const allMedalWinnersofIndia = data.reduce((allMedalWinners, obj) => {
    if (obj["Team"] == "India" && obj["Medal"] != "NA") {
      if (allMedalWinners.hasOwnProperty([obj["Season"]])) {
        allMedalWinners[obj["Season"]].push(obj);
      } else {
        allMedalWinners[obj["Season"]] = [obj];
      }
    }
    return allMedalWinners;
  }, {});
  return allMedalWinnersofIndia;
}

// console.log(allMedalWinnersFromIndia());
